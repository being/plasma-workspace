# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-14 01:39+0000\n"
"PO-Revision-Date: 2022-08-19 15:32-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: contents/ui/main.qml:71
#, kde-format
msgid "Switch to activity %1"
msgstr "Alternar para a atividade: %1"

#: contents/ui/main.qml:91
#, kde-format
msgctxt "@action:inmenu"
msgid "&Configure Activities…"
msgstr "&Configurar atividades..."
