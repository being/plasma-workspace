# Albanian translation for kdebase-workspace
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the kdebase-workspace package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: kdebase-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-12 00:40+0000\n"
"PO-Revision-Date: 2009-10-21 13:14+0000\n"
"Last-Translator: Vilson Gjeci <vilsongjeci@gmail.com>\n"
"Language-Team: Albanian <sq@li.org>\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2011-05-06 01:58+0000\n"
"X-Generator: Launchpad (build 12959)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/ui/BatteryItem.qml:100
#, kde-format
msgid "Charging"
msgstr ""

#: package/contents/ui/BatteryItem.qml:102
#, kde-format
msgid "Discharging"
msgstr ""

#: package/contents/ui/BatteryItem.qml:104 package/contents/ui/main.qml:123
#, kde-format
msgid "Fully Charged"
msgstr ""

#: package/contents/ui/BatteryItem.qml:106
#, kde-format
msgid "Not Charging"
msgstr ""

#: package/contents/ui/BatteryItem.qml:109
#, kde-format
msgctxt "Battery is currently not present in the bay"
msgid "Not present"
msgstr ""

#: package/contents/ui/BatteryItem.qml:121
#, kde-format
msgctxt "Placeholder is battery percentage"
msgid "%1%"
msgstr ""

#: package/contents/ui/BatteryItem.qml:185
#, kde-format
msgid ""
"This battery's health is at only %1% and it should be replaced. Contact the "
"manufacturer."
msgstr ""

#: package/contents/ui/BatteryItem.qml:204
#, kde-format
msgid "Time To Full:"
msgstr ""

#: package/contents/ui/BatteryItem.qml:205
#, kde-format
msgid "Remaining Time:"
msgstr ""

#: package/contents/ui/BatteryItem.qml:211
#, kde-format
msgctxt "@info"
msgid "Estimating…"
msgstr ""

#: package/contents/ui/BatteryItem.qml:221
#, kde-format
msgid "Battery Health:"
msgstr ""

#: package/contents/ui/BatteryItem.qml:227
#, kde-format
msgctxt "Placeholder is battery health percentage"
msgid "%1%"
msgstr ""

#: package/contents/ui/BatteryItem.qml:239
#, kde-format
msgid "Battery is configured to charge up to approximately %1%."
msgstr ""

#: package/contents/ui/CompactRepresentation.qml:106
#: package/contents/ui/CompactRepresentation.qml:150
#, kde-format
msgctxt "battery percentage below battery icon"
msgid "%1%"
msgstr ""

#: package/contents/ui/main.qml:75
#, kde-format
msgid "The battery applet has enabled suppressing sleep and screen locking"
msgstr ""

#: package/contents/ui/main.qml:97
#: package/contents/ui/PowerManagementItem.qml:51
#: package/contents/ui/PowerProfileItem.qml:75
#, fuzzy, kde-format
#| msgid "Power Management"
msgid "Power Management"
msgstr "Menaxhimi i Tensionit"

#: package/contents/ui/main.qml:97
#, fuzzy, kde-format
#| msgid "Power Management"
msgid "Power and Battery"
msgstr "Menaxhimi i Tensionit"

#: package/contents/ui/main.qml:117
#, kde-format
msgid "Battery is not present in the bay"
msgstr ""

#: package/contents/ui/main.qml:130
#, kde-format
msgid "Battery at %1%, not Charging"
msgstr ""

#: package/contents/ui/main.qml:132
#, kde-format
msgid "Battery at %1%, plugged in but still discharging"
msgstr ""

#: package/contents/ui/main.qml:134
#, kde-format
msgid "Battery at %1%, Charging"
msgstr ""

#: package/contents/ui/main.qml:137
#, kde-format
msgid "Battery at %1%"
msgstr ""

#: package/contents/ui/main.qml:145
#, kde-format
msgid "The power supply is not powerful enough to charge the battery"
msgstr ""

#: package/contents/ui/main.qml:149
#, kde-format
msgid "No Batteries Available"
msgstr ""

#: package/contents/ui/main.qml:156
#, kde-format
msgctxt "time until fully charged - HH:MM"
msgid "%1 until fully charged"
msgstr ""

#: package/contents/ui/main.qml:158
#, kde-format
msgctxt "remaining time left of battery usage - HH:MM"
msgid "%1 remaining"
msgstr ""

#: package/contents/ui/main.qml:161
#, kde-format
msgid "Not charging"
msgstr ""

#: package/contents/ui/main.qml:166
#, kde-format
msgid ""
"Automatic sleep and screen locking are disabled; middle-click to re-enable"
msgstr ""

#: package/contents/ui/main.qml:168
#, kde-format
msgid "Middle-click to disable automatic sleep and screen locking"
msgstr ""

#: package/contents/ui/main.qml:173
#, kde-format
msgid "An application has requested activating Performance mode"
msgid_plural "%1 applications have requested activating Performance mode"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/main.qml:177
#, kde-format
msgid "System is in Performance mode; scroll to change"
msgstr ""

#: package/contents/ui/main.qml:181
#, kde-format
msgid "An application has requested activating Power Save mode"
msgid_plural "%1 applications have requested activating Power Save mode"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/main.qml:185
#, kde-format
msgid "System is in Power Save mode; scroll to change"
msgstr ""

#: package/contents/ui/main.qml:188
#, kde-format
msgid "System is in Balanced Power mode; scroll to change"
msgstr ""

#: package/contents/ui/main.qml:300
#, fuzzy, kde-format
#| msgid "Show charge &information"
msgid "&Show Energy Information…"
msgstr "Shfaq &informacionin e karikimit"

#: package/contents/ui/main.qml:306
#, kde-format
msgid "Show Battery Percentage on Icon When Not Fully Charged"
msgstr ""

#: package/contents/ui/main.qml:319
#, fuzzy, kde-format
#| msgid "Power Management"
msgid "&Configure Power Management…"
msgstr "Menaxhimi i Tensionit"

#: package/contents/ui/PowerManagementItem.qml:44
#, kde-format
msgctxt "@title:group"
msgid "Sleep and Screen Locking after Inactivity"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:89
#, kde-format
msgctxt "Sleep and Screen Locking after Inactivity"
msgid "Blocked"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:89
#, kde-format
msgctxt "Sleep and Screen Locking after Inactivity"
msgid "Automatic"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:99
#, kde-format
msgctxt ""
"@option:check Manually block sleep and screen locking after inactivity"
msgid "Manually block"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:133
#, kde-format
msgid "Failed to unblock automatic sleep and screen locking"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:136
#, kde-format
msgid "Failed to block automatic sleep and screen locking"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:174
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid ""
"Your laptop is configured not to sleep when closing the lid while an "
"external monitor is connected."
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:185
#, kde-format
msgid "%1 application is currently blocking sleep and screen locking:"
msgid_plural "%1 applications are currently blocking sleep and screen locking:"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/PowerManagementItem.qml:205
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (%2)"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:207
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (unknown reason)"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:209
#, kde-format
msgid "An application is currently blocking sleep and screen locking (%1)"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:211
#, kde-format
msgid ""
"An application is currently blocking sleep and screen locking (unknown "
"reason)"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:215
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: %2"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:217
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: unknown reason"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:219
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: %1"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:221
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: unknown reason"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:39
#, kde-format
msgid "Power Save"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:43
#, kde-format
msgid "Balanced"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:47
#, kde-format
msgid "Performance"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:64
#, kde-format
msgid "Power Profile"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:107
#, kde-format
msgctxt "Power profile"
msgid "Not available"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:143
#, kde-format
msgid "Failed to activate %1 mode"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:220
#, kde-format
msgid ""
"Performance mode has been disabled to reduce heat generation because the "
"computer has detected that it may be sitting on your lap."
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:222
#, kde-format
msgid ""
"Performance mode is unavailable because the computer is running too hot."
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:224
#, fuzzy, kde-format
#| msgid "Power Management"
msgid "Performance mode is unavailable."
msgstr "Menaxhimi i Tensionit"

#: package/contents/ui/PowerProfileItem.qml:237
#, kde-format
msgid ""
"Performance may be lowered to reduce heat generation because the computer "
"has detected that it may be sitting on your lap."
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:239
#, kde-format
msgid "Performance may be reduced because the computer is running too hot."
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:241
#, fuzzy, kde-format
#| msgid "Power Management"
msgid "Performance may be reduced."
msgstr "Menaxhimi i Tensionit"

#: package/contents/ui/PowerProfileItem.qml:252
#, kde-format
msgid "One application has requested activating %2:"
msgid_plural "%1 applications have requested activating %2:"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/PowerProfileItem.qml:270
#, kde-format
msgctxt ""
"%1 is the name of the application, %2 is the reason provided by it for "
"activating performance mode"
msgid "%1: %2"
msgstr ""

#: package/contents/ui/PowerProfileItem.qml:289
#, kde-kuit-format
msgid ""
"Power profiles may be supported on your device.<nl/>Try installing the "
"<command>power-profiles-daemon</command> package using your distribution's "
"package manager and restarting the system."
msgstr ""

#, fuzzy
#~| msgid "Power Management"
#~ msgid "Performance mode has been manually enabled"
#~ msgstr "Menaxhimi i Tensionit"

#~ msgid "General"
#~ msgstr "Të Përgjithshme"

#~ msgid "Configure Battery Monitor"
#~ msgstr "Konfiguro Monitorin e Baterisë"

#~ msgid "Show the state for &each battery present"
#~ msgstr "Shfaq gjendjen për &çdo bateri të pranishme"

#, fuzzy
#~| msgid "Show the state for &each battery present"
#~ msgid "Show remaining time for the battery"
#~ msgstr "Shfaq gjendjen për &çdo bateri të pranishme"
