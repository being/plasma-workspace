# Translation of plasma_applet_org.kde.plasma.devicenotifier.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2007, 2008, 2009, 2010, 2011, 2012, 2014, 2016.
# Dalibor Djuric <daliborddjuric@gmail.com>, 2009, 2010, 2011.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.devicenotifier\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2016-07-10 19:45+0200\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#: package/contents/ui/DeviceItem.qml:188
#, fuzzy, kde-format
#| msgctxt "@info:status Free disk space"
#| msgid "%1 free"
msgctxt "@info:status Free disk space"
msgid "%1 free of %2"
msgstr "%1 слободно"

#: package/contents/ui/DeviceItem.qml:192
#, fuzzy, kde-format
#| msgctxt ""
#| "Accessing is a less technical word for Mounting; translation should be "
#| "short and mean 'Currently mounting this device'"
#| msgid "Accessing..."
msgctxt ""
"Accessing is a less technical word for Mounting; translation should be short "
"and mean 'Currently mounting this device'"
msgid "Accessing…"
msgstr "Приступам..."

#: package/contents/ui/DeviceItem.qml:195
#, fuzzy, kde-format
#| msgctxt ""
#| "Removing is a less technical word for Unmounting; translation shoud be "
#| "short and mean 'Currently unmounting this device'"
#| msgid "Removing..."
msgctxt ""
"Removing is a less technical word for Unmounting; translation should be "
"short and mean 'Currently unmounting this device'"
msgid "Removing…"
msgstr "Уклањам..."

#: package/contents/ui/DeviceItem.qml:198
#, kde-format
msgid "Don't unplug yet! Files are still being transferred…"
msgstr ""

#: package/contents/ui/DeviceItem.qml:229
#, kde-format
msgid "Open in File Manager"
msgstr ""

#: package/contents/ui/DeviceItem.qml:232
#, kde-format
msgid "Mount and Open"
msgstr ""

#: package/contents/ui/DeviceItem.qml:234
#, kde-format
msgid "Eject"
msgstr ""

#: package/contents/ui/DeviceItem.qml:236
#, kde-format
msgid "Safely remove"
msgstr ""

#: package/contents/ui/DeviceItem.qml:278
#, kde-format
msgid "Mount"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:43
#: package/contents/ui/main.qml:225
#, kde-format
msgid "Remove All"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:44
#, fuzzy, kde-format
#| msgid "Click to safely remove this device."
msgid "Click to safely remove all devices"
msgstr "Кликните да безбедно уклоните овај уређај."

#: package/contents/ui/FullRepresentation.qml:186
#, fuzzy, kde-format
#| msgid "Non-removable devices only"
msgid "No removable devices attached"
msgstr "само неуклоњиве уређаје"

#: package/contents/ui/FullRepresentation.qml:186
#, fuzzy, kde-format
#| msgid "No Devices Available"
msgid "No disks available"
msgstr "Нема доступних уређаја"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "Most Recent Device"
msgstr "Последњи уређај"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "No Devices Available"
msgstr "Нема доступних уређаја"

#: package/contents/ui/main.qml:207
#, fuzzy, kde-format
#| msgctxt "Open auto mounter kcm"
#| msgid "Configure Removable Devices"
msgctxt "Open auto mounter kcm"
msgid "Configure Removable Devices…"
msgstr "Подеси уклоњиве уређаје"

#: package/contents/ui/main.qml:233
#, fuzzy, kde-format
#| msgid "Removable devices only"
msgid "Removable Devices"
msgstr "само уклоњиве уређаје"

#: package/contents/ui/main.qml:248
#, fuzzy, kde-format
#| msgctxt "Open auto mounter kcm"
#| msgid "Configure Removable Devices"
msgid "Non Removable Devices"
msgstr "Подеси уклоњиве уређаје"

#: package/contents/ui/main.qml:263
#, fuzzy, kde-format
#| msgid "All devices"
msgid "All Devices"
msgstr "све уређаје"

#: package/contents/ui/main.qml:280
#, fuzzy, kde-format
#| msgid "Open popup when new device is plugged in"
msgid "Show popup when new device is plugged in"
msgstr "Прикажи искакач кад се утакне нови уређај"

#: plugin/ksolidnotify.cpp:190
#, kde-format
msgid "Device Status"
msgstr ""

#: plugin/ksolidnotify.cpp:190
#, kde-format
msgid "A device can now be safely removed"
msgstr ""

#: plugin/ksolidnotify.cpp:191
#, kde-format
msgid "This device can now be safely removed."
msgstr ""

#: plugin/ksolidnotify.cpp:198
#, fuzzy, kde-format
#| msgid "It is currently safe to remove this device."
msgid "You are not authorized to mount this device."
msgstr "Тренутно је безбедно уклонити овај уређај."

#: plugin/ksolidnotify.cpp:201
#, fuzzy, kde-format
#| msgid "It is currently safe to remove this device."
msgctxt "Remove is less technical for unmount"
msgid "You are not authorized to remove this device."
msgstr "Тренутно је безбедно уклонити овај уређај."

#: plugin/ksolidnotify.cpp:204
#, fuzzy, kde-format
#| msgid "Click to eject this disc."
msgid "You are not authorized to eject this disc."
msgstr "Кликните за избацивање овог диска."

#: plugin/ksolidnotify.cpp:211
#, kde-format
msgid "Could not mount this device as it is busy."
msgstr ""

#: plugin/ksolidnotify.cpp:242
#, fuzzy, kde-format
#| msgid "Click to access this device from other applications."
msgid "One or more files on this device are open within an application."
msgstr "Кликните да приступите овом уређају из других програма."

#: plugin/ksolidnotify.cpp:244
#, fuzzy, kde-format
#| msgid "Click to access this device from other applications."
msgid "One or more files on this device are opened in application \"%2\"."
msgid_plural ""
"One or more files on this device are opened in following applications: %2."
msgstr[0] "Кликните да приступите овом уређају из других програма."
msgstr[1] "Кликните да приступите овом уређају из других програма."
msgstr[2] "Кликните да приступите овом уређају из других програма."
msgstr[3] "Кликните да приступите овом уређају из других програма."

#: plugin/ksolidnotify.cpp:247
#, kde-format
msgctxt "separator in list of apps blocking device unmount"
msgid ", "
msgstr ""

#: plugin/ksolidnotify.cpp:265
#, fuzzy, kde-format
#| msgid "Click to safely remove this device."
msgid "Could not mount this device."
msgstr "Кликните да безбедно уклоните овај уређај."

#: plugin/ksolidnotify.cpp:268
#, fuzzy, kde-format
#| msgid "Click to safely remove this device."
msgctxt "Remove is less technical for unmount"
msgid "Could not remove this device."
msgstr "Кликните да безбедно уклоните овај уређај."

#: plugin/ksolidnotify.cpp:271
#, fuzzy, kde-format
#| msgid "Click to eject this disc."
msgid "Could not eject this disc."
msgstr "Кликните за избацивање овог диска."

#~ msgid "General"
#~ msgstr "Опште"

#~ msgid ""
#~ "It is currently <b>not safe</b> to remove this device: applications may "
#~ "be accessing it. Click the eject button to safely remove this device."
#~ msgstr ""
#~ "Тренутно <b>није безбедно</b> уклонити овај уређај, јер му програми можда "
#~ "приступају. Кликните на дугме за избацивање да га безбедно уклоните."

#~ msgid "This device is currently accessible."
#~ msgstr "Овом уређају се тренутно може приступити."

#~ msgid ""
#~ "It is currently <b>not safe</b> to remove this device: applications may "
#~ "be accessing other volumes on this device. Click the eject button on "
#~ "these other volumes to safely remove this device."
#~ msgstr ""
#~ "Тренутно <b>није безбедно</b> уклонити овај уређај, јер програми можда "
#~ "приступају другим складиштима на њему. Кликните на дугме за избацивање на "
#~ "тим другим складиштима да безбедно уклоните овај уређај."

#~ msgid "This device is not currently accessible."
#~ msgstr "Овом уређају се тренутно не може приступити."

#~ msgid "1 action for this device"
#~ msgid_plural "%1 actions for this device"
#~ msgstr[0] "%1 радња за овај уређај"
#~ msgstr[1] "%1 радње за овај уређај"
#~ msgstr[2] "%1 радњи за овај уређај"
#~ msgstr[3] "радња за овај уређај"
