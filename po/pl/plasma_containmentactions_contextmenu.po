# translation of plasma_containmentactions_contextmenu.po to Polish
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marta Rybczynska <kde-i18n@rybczynska.net>, 2010.
# SPDX-FileCopyrightText: 2014, 2015, 2019, 2021, 2022, 2024 Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma_containmentactions_contextmenu\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-26 00:39+0000\n"
"PO-Revision-Date: 2024-04-01 08:11+0200\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#: menu.cpp:102
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Show KRunner"
msgstr "Pokaż KRunnera"

#: menu.cpp:107
#, kde-format
msgid "Open Terminal"
msgstr "Otwórz osobne okno terminala"

#: menu.cpp:111
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Lock Screen"
msgstr "Zablokuj ekran"

#: menu.cpp:120
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Leave…"
msgstr "Wyjdź..."

#: menu.cpp:125
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Display Configuration"
msgstr "Ustawienia wyświetlacza"

#: menu.cpp:285
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Configure Contextual Menu Plugin"
msgstr "Ustawienia wtyczki menu podręcznego"

#: menu.cpp:295
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Other Actions]"
msgstr "[Inne działania]"

#: menu.cpp:298
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Wallpaper Actions"
msgstr "Działania związane z tapetą"

#: menu.cpp:302
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Separator]"
msgstr "[Rozgranicznik]"

#~ msgctxt "plasma_containmentactions_contextmenu"
#~ msgid "Configure Display Settings…"
#~ msgstr "Ustawienia wyświetlacza..."

#~ msgctxt "plasma_containmentactions_contextmenu"
#~ msgid "Run Command..."
#~ msgstr "Wykonaj polecenie..."

#~ msgid "Add Panel"
#~ msgstr "Dodaj panel"
