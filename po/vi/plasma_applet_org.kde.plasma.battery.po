# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Lê Hoàng Phương <herophuong93@gmail.com>, 2012, 2013.
# Phu Hung Nguyen <phu.nguyen@kdemail.net>, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-12 00:40+0000\n"
"PO-Revision-Date: 2023-01-08 09:43+0100\n"
"Last-Translator: Phu Hung Nguyen <phu.nguyen@kdemail.net>\n"
"Language-Team: Vietnamese <kde-l10n-vi@kde.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 22.12.0\n"

#: package/contents/ui/BatteryItem.qml:100
#, kde-format
msgid "Charging"
msgstr "Đang sạc"

#: package/contents/ui/BatteryItem.qml:102
#, kde-format
msgid "Discharging"
msgstr "Đang xả"

#: package/contents/ui/BatteryItem.qml:104 package/contents/ui/main.qml:123
#, kde-format
msgid "Fully Charged"
msgstr "Đã sạc đầy"

#: package/contents/ui/BatteryItem.qml:106
#, kde-format
msgid "Not Charging"
msgstr "Đang không sạc"

#: package/contents/ui/BatteryItem.qml:109
#, kde-format
msgctxt "Battery is currently not present in the bay"
msgid "Not present"
msgstr "Không có"

#: package/contents/ui/BatteryItem.qml:121
#, kde-format
msgctxt "Placeholder is battery percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:185
#, kde-format
msgid ""
"This battery's health is at only %1% and it should be replaced. Contact the "
"manufacturer."
msgstr ""
"Sức chứa của cục pin này chỉ còn %1%, nó nên được thay. Hãy liên hệ với nhà "
"sản xuất."

#: package/contents/ui/BatteryItem.qml:204
#, kde-format
msgid "Time To Full:"
msgstr "Thời gian đến khi đầy:"

#: package/contents/ui/BatteryItem.qml:205
#, kde-format
msgid "Remaining Time:"
msgstr "Thời gian còn lại"

#: package/contents/ui/BatteryItem.qml:211
#, kde-format
msgctxt "@info"
msgid "Estimating…"
msgstr "Đang ước tính..."

#: package/contents/ui/BatteryItem.qml:221
#, kde-format
msgid "Battery Health:"
msgstr "Sức chứa của pin:"

#: package/contents/ui/BatteryItem.qml:227
#, kde-format
msgctxt "Placeholder is battery health percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:239
#, kde-format
msgid "Battery is configured to charge up to approximately %1%."
msgstr "Pin được cấu hình để sạc đến khoảng %1%."

#: package/contents/ui/CompactRepresentation.qml:106
#: package/contents/ui/CompactRepresentation.qml:150
#, kde-format
msgctxt "battery percentage below battery icon"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/main.qml:75
#, fuzzy, kde-format
#| msgid "An application is preventing sleep and screen locking:"
msgid "The battery applet has enabled suppressing sleep and screen locking"
msgstr "Một ứng dụng đang ngăn cản việc ngủ và khoá màn hình:"

#: package/contents/ui/main.qml:97
#: package/contents/ui/PowerManagementItem.qml:51
#: package/contents/ui/PowerProfileItem.qml:75
#, kde-format
msgid "Power Management"
msgstr "Quản lí nguồn điện"

#: package/contents/ui/main.qml:97
#, fuzzy, kde-format
#| msgid "Power Management"
msgid "Power and Battery"
msgstr "Quản lí nguồn điện"

#: package/contents/ui/main.qml:117
#, kde-format
msgid "Battery is not present in the bay"
msgstr ""

#: package/contents/ui/main.qml:130
#, kde-format
msgid "Battery at %1%, not Charging"
msgstr "Pin ở mức %1%, đang không sạc"

#: package/contents/ui/main.qml:132
#, kde-format
msgid "Battery at %1%, plugged in but still discharging"
msgstr "Pin ở mức %1%, cắm nhưng vẫn đang xả"

#: package/contents/ui/main.qml:134
#, kde-format
msgid "Battery at %1%, Charging"
msgstr "Pin ở mức %1%, đang sạc"

#: package/contents/ui/main.qml:137
#, kde-format
msgid "Battery at %1%"
msgstr "Pin ở mức %1%"

#: package/contents/ui/main.qml:145
#, kde-format
msgid "The power supply is not powerful enough to charge the battery"
msgstr "Nguồn điện không đủ mạnh để sạc được pin này"

#: package/contents/ui/main.qml:149
#, kde-format
msgid "No Batteries Available"
msgstr "Hiện không có pin nào"

#: package/contents/ui/main.qml:156
#, kde-format
msgctxt "time until fully charged - HH:MM"
msgid "%1 until fully charged"
msgstr "Còn %1 đến khi sạc đầy"

#: package/contents/ui/main.qml:158
#, kde-format
msgctxt "remaining time left of battery usage - HH:MM"
msgid "%1 remaining"
msgstr "Còn được %1"

#: package/contents/ui/main.qml:161
#, kde-format
msgid "Not charging"
msgstr "Đang không sạc"

#: package/contents/ui/main.qml:166
#, fuzzy, kde-format
#| msgid "Automatic sleep and screen locking are disabled"
msgid ""
"Automatic sleep and screen locking are disabled; middle-click to re-enable"
msgstr "Việc tự động ngủ và khoá màn hình đã bị tắt"

#: package/contents/ui/main.qml:168
#, fuzzy, kde-format
#| msgctxt "Minimize the length of this string as much as possible"
#| msgid "Manually block sleep and screen locking"
msgid "Middle-click to disable automatic sleep and screen locking"
msgstr "Ngăn cản thủ công việc ngủ và khoá màn hình"

#: package/contents/ui/main.qml:173
#, kde-format
msgid "An application has requested activating Performance mode"
msgid_plural "%1 applications have requested activating Performance mode"
msgstr[0] "%1 ứng dụng đã yêu cầu kích hoạt chế độ \"Ưu tiên hiệu năng\""

#: package/contents/ui/main.qml:177
#, fuzzy, kde-format
#| msgid "System is in Performance mode"
msgid "System is in Performance mode; scroll to change"
msgstr "Hệ thống đang trong chế độ \"Ưu tiên hiệu năng\""

#: package/contents/ui/main.qml:181
#, kde-format
msgid "An application has requested activating Power Save mode"
msgid_plural "%1 applications have requested activating Power Save mode"
msgstr[0] "%1 ứng dụng đã yêu cầu kích hoạt chế độ \"Tiết kiệm điện\""

#: package/contents/ui/main.qml:185
#, fuzzy, kde-format
#| msgid "System is in Power Save mode"
msgid "System is in Power Save mode; scroll to change"
msgstr "Hệ thống đang trong chế độ \"Tiết kiệm điện\""

#: package/contents/ui/main.qml:188
#, kde-format
msgid "System is in Balanced Power mode; scroll to change"
msgstr ""

#: package/contents/ui/main.qml:300
#, kde-format
msgid "&Show Energy Information…"
msgstr "&Hiện thông tin năng lượng…"

#: package/contents/ui/main.qml:306
#, kde-format
msgid "Show Battery Percentage on Icon When Not Fully Charged"
msgstr "Hiện phần trăm pin trên biểu tượng khi chưa sạc đầy"

#: package/contents/ui/main.qml:319
#, fuzzy, kde-format
#| msgid "&Configure Energy Saving…"
msgid "&Configure Power Management…"
msgstr "&Cấu hình việc tiết kiệm năng lượng…"

#: package/contents/ui/PowerManagementItem.qml:44
#, kde-format
msgctxt "@title:group"
msgid "Sleep and Screen Locking after Inactivity"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:89
#, kde-format
msgctxt "Sleep and Screen Locking after Inactivity"
msgid "Blocked"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:89
#, kde-format
msgctxt "Sleep and Screen Locking after Inactivity"
msgid "Automatic"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:99
#, kde-format
msgctxt ""
"@option:check Manually block sleep and screen locking after inactivity"
msgid "Manually block"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:133
#, fuzzy, kde-format
#| msgctxt "Minimize the length of this string as much as possible"
#| msgid "Manually block sleep and screen locking"
msgid "Failed to unblock automatic sleep and screen locking"
msgstr "Ngăn cản thủ công việc ngủ và khoá màn hình"

#: package/contents/ui/PowerManagementItem.qml:136
#, fuzzy, kde-format
#| msgctxt "Minimize the length of this string as much as possible"
#| msgid "Manually block sleep and screen locking"
msgid "Failed to block automatic sleep and screen locking"
msgstr "Ngăn cản thủ công việc ngủ và khoá màn hình"

#: package/contents/ui/PowerManagementItem.qml:174
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid ""
"Your laptop is configured not to sleep when closing the lid while an "
"external monitor is connected."
msgstr ""
"Máy của bạn được cấu hình để không ngủ khi đóng nắp trong khi đang kết nối "
"với một màn hình ngoài."

#: package/contents/ui/PowerManagementItem.qml:185
#, kde-format
msgid "%1 application is currently blocking sleep and screen locking:"
msgid_plural "%1 applications are currently blocking sleep and screen locking:"
msgstr[0] "%1 ứng dụng đang ngăn cản việc ngủ và khoá màn hình:"

#: package/contents/ui/PowerManagementItem.qml:205
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (%2)"
msgstr "%1 đang ngăn cản việc ngủ và khoá màn hình (%2)"

#: package/contents/ui/PowerManagementItem.qml:207
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (unknown reason)"
msgstr "%1 đang ngăn cản việc ngủ và khoá màn hình (không rõ lí do)"

#: package/contents/ui/PowerManagementItem.qml:209
#, fuzzy, kde-format
#| msgid "%1 application is currently blocking sleep and screen locking:"
#| msgid_plural ""
#| "%1 applications are currently blocking sleep and screen locking:"
msgid "An application is currently blocking sleep and screen locking (%1)"
msgstr "%1 ứng dụng đang ngăn cản việc ngủ và khoá màn hình:"

#: package/contents/ui/PowerManagementItem.qml:211
#, fuzzy, kde-format
#| msgid "%1 is currently blocking sleep and screen locking (unknown reason)"
msgid ""
"An application is currently blocking sleep and screen locking (unknown "
"reason)"
msgstr "%1 đang ngăn cản việc ngủ và khoá màn hình (không rõ lí do)"

#: package/contents/ui/PowerManagementItem.qml:215
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerManagementItem.qml:217
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: unknown reason"
msgstr "%1: không rõ lí do"

#: package/contents/ui/PowerManagementItem.qml:219
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: %1"
msgstr ""

#: package/contents/ui/PowerManagementItem.qml:221
#, fuzzy, kde-format
#| msgctxt "Application name: reason for preventing sleep and screen locking"
#| msgid "%1: unknown reason"
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: unknown reason"
msgstr "%1: không rõ lí do"

#: package/contents/ui/PowerProfileItem.qml:39
#, kde-format
msgid "Power Save"
msgstr "Tiết kiệm điện"

#: package/contents/ui/PowerProfileItem.qml:43
#, kde-format
msgid "Balanced"
msgstr "Cân bằng"

#: package/contents/ui/PowerProfileItem.qml:47
#, kde-format
msgid "Performance"
msgstr "Hiệu năng"

#: package/contents/ui/PowerProfileItem.qml:64
#, kde-format
msgid "Power Profile"
msgstr "Hồ sơ nguồn điện"

#: package/contents/ui/PowerProfileItem.qml:107
#, fuzzy, kde-format
#| msgid "No Batteries Available"
msgctxt "Power profile"
msgid "Not available"
msgstr "Hiện không có pin nào"

#: package/contents/ui/PowerProfileItem.qml:143
#, kde-format
msgid "Failed to activate %1 mode"
msgstr "Kích hoạt chế độ %1 thất bại"

#: package/contents/ui/PowerProfileItem.qml:220
#, kde-format
msgid ""
"Performance mode has been disabled to reduce heat generation because the "
"computer has detected that it may be sitting on your lap."
msgstr ""
"Chế độ hiệu năng đã bị tắt để giảm việc sinh nhiệt vì máy tính nhận thấy "
"rằng nó có thể đang ở trên đùi bạn."

#: package/contents/ui/PowerProfileItem.qml:222
#, kde-format
msgid ""
"Performance mode is unavailable because the computer is running too hot."
msgstr "Chế độ hiệu năng không khả dụng vì máy tính đang chạy quá nóng."

#: package/contents/ui/PowerProfileItem.qml:224
#, kde-format
msgid "Performance mode is unavailable."
msgstr "Chế độ hiệu năng không khả dụng."

#: package/contents/ui/PowerProfileItem.qml:237
#, kde-format
msgid ""
"Performance may be lowered to reduce heat generation because the computer "
"has detected that it may be sitting on your lap."
msgstr ""
"Hiệu năng có thể bị hạ thấp để giảm việc sinh nhiệt vì máy tính nhận thấy "
"rằng nó có thể đang ở trên đùi bạn."

#: package/contents/ui/PowerProfileItem.qml:239
#, kde-format
msgid "Performance may be reduced because the computer is running too hot."
msgstr "Hiệu năng có thể bị suy giảm vì máy tính đang chạy quá nóng."

#: package/contents/ui/PowerProfileItem.qml:241
#, kde-format
msgid "Performance may be reduced."
msgstr "Hiệu năng có thể bị suy giảm."

#: package/contents/ui/PowerProfileItem.qml:252
#, kde-format
msgid "One application has requested activating %2:"
msgid_plural "%1 applications have requested activating %2:"
msgstr[0] "%1 ứng dụng đã yêu cầu kích hoạt %2:"

#: package/contents/ui/PowerProfileItem.qml:270
#, kde-format
msgctxt ""
"%1 is the name of the application, %2 is the reason provided by it for "
"activating performance mode"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerProfileItem.qml:289
#, kde-kuit-format
msgid ""
"Power profiles may be supported on your device.<nl/>Try installing the "
"<command>power-profiles-daemon</command> package using your distribution's "
"package manager and restarting the system."
msgstr ""

#, fuzzy
#~| msgid "Battery at %1%"
#~ msgctxt "Placeholder is the battery number"
#~ msgid "Battery %1"
#~ msgstr "Pin ở mức %1%"

#~ msgid "Battery"
#~ msgstr "Pin"

#~ msgid "The battery applet has enabled system-wide inhibition"
#~ msgstr "Tiểu ứng dụng pin đã bật điều khiển ức chế toàn hệ thống"

#~ msgctxt "Minimize the length of this string as much as possible"
#~ msgid "Manually block sleep and screen locking"
#~ msgstr "Ngăn cản thủ công việc ngủ và khoá màn hình"

#~ msgctxt "Placeholder is brightness percentage"
#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Battery and Brightness"
#~ msgstr "Pin và độ sáng"

#~ msgid "Brightness"
#~ msgstr "Độ sáng"

#~ msgid "Scroll to adjust screen brightness"
#~ msgstr "Cuộn để chỉnh độ sáng màn hình"

#~ msgid "Display Brightness"
#~ msgstr "Độ sáng màn hình"

#~ msgid "Keyboard Brightness"
#~ msgstr "Độ sáng bàn phím"

#, fuzzy
#~| msgid "Performance mode is unavailable."
#~ msgid "Performance mode has been manually enabled"
#~ msgstr "Chế độ hiệu năng không khả dụng."

#~ msgid ""
#~ "Performance mode is unavailable because the computer has detected it is "
#~ "sitting on your lap."
#~ msgstr ""
#~ "Chế độ hiệu năng không khả dụng vì máy tính nhận thấy rằng nó đang ở trên "
#~ "đùi bạn."

#~ msgid "General"
#~ msgstr "Chung"

#~ msgctxt "short symbol to signal there is no battery currently available"
#~ msgid "-"
#~ msgstr "-"
