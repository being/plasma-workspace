# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Martin Schlander <mschlander@opensuse.org>, 2009, 2010, 2016, 2017, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_calculatorrunner\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-03 01:39+0000\n"
"PO-Revision-Date: 2021-09-27 20:54+0200\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.04.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: calculatorrunner.cpp:28
#, kde-format
msgid "Copy to Clipboard"
msgstr "Kopiér til udklipsholderen"

#: calculatorrunner.cpp:31
#, kde-format
msgid ""
"Calculates the value of :q: when :q: is made up of numbers and mathematical "
"symbols such as +, -, /, *, ! and ^."
msgstr ""
"Beregner værdien af :q: når :q: udgøres af tal og matematiske symboler såsom "
"+, -, /, *, ! og ^."

#: calculatorrunner.cpp:36
#, kde-format
msgid "Enter a common math function"
msgstr ""

#: calculatorrunner.cpp:140
#, kde-format
msgctxt "The result of the calculation is only an approximation"
msgid "Approximation"
msgstr "Tilnærmelse"

#~ msgid ""
#~ "The exchange rates could not be updated. The following error has been "
#~ "reported: %1"
#~ msgstr ""
#~ "Valutakurserne kunne ikke opdateres. Følgende fejl blev rapporteret: %1"
